<?php
$tasks = ["Get git", "Bake HTML", "Eat CSS", "Learn PHP"];

if (isset($_GET["index"])) {
  // will check if it can GET any data passed via url (specifically in this case, data labeled 'index'), if it can't find an index, nothing will happen
  $indexGet = $_GET["index"]; //contains the value of 'index'
  echo "The retrieved task from GET is $tasks[$indexGet]."; //shows the task with same index chosen from the below select

  //not similar to fetch request or api requests at all
  //the method above passes data through the url; isset will look for any data sent with a method get
  // $indexGet - new variable
}

if (isset($_POST["index"])) {
  //will receive the data submitted via POST

  $indexGet = $_POST["index"]; //contains the value of 'index'
  echo "The retrieved task from POST is $tasks[$indexGet]."; //shows the task with same index chosen from the below select

  //POST doesn't send data through the URL; this example produces the same output but there is not data in the url
}

// when submitting form data the difference between get and post is that get send the data via the url (which will be visible to the user) POST does not (therefore more secure)
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S5: Client-Server Communication (GET & POST)</title>
</head>
<body>
  <h1>Task Index from GET</h1>
  <form method='GET'>
    <select name="index" required>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
    </select>

    <button type='submit'>GET</button>
  </form>
  <!-- form, method of get, select has a name, values = can pass submitted form information to the URL  
  should note method get and name of the input 
  will only send the get request to itself 
-->

<h1>Task Index from POST</h1>
  <form method='POST'>
    <select name="index" required>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
    </select>

    <button type='submit'>POST</button>
  </form>
</body>
</html>