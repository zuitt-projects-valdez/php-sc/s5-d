<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S5: Activity</title>
</head>
<body>
  <?php session_start();?>

  <?php if(isset($_SESSION['user'])):?>
    <?php foreach($_SESSION['user'] as $index => $user): ?>
    <p>
      Hello <?= $user->username ?> welcome back. 
    </p>
    <br>
    <form action="./server.php" method='POST'>
      <input type="hidden" name='action' value='logout'>
      <button type='submit'>Logout</button>
    </form>
    
    <?php endforeach;?>
    <?php else: ?>
      <form action="./server.php" method='POST'>
        <input type="hidden" name='action' value='login'>
        Username: <input type="text" name='username' required>
        Password: <input type="text" name='password' required>
        <button type='submit'>Login</button>
      </form>

    <?php endif;?>
</body>
</html>