<?php

session_start(); 

class Login{
  public function login($username, $password){
    $newUser = (object)[
      'username'=>$username, 
      'password'=>$password
    ]; 

    if($_SESSION['user'] === null){
      $_SESSION['user'] = array();
    }
    array_push($_SESSION['user'], $newUser);
  }

  public function clear(){
    session_destroy();
  }
}

$login = new Login(); 

if($_POST['action']==='login'){
  $login->login($_POST['username'], $_POST['password']);
}else if($_POST['action']==='logout'){
  $login->clear(); 
}

header('Location: ./index.php');